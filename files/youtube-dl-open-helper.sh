#!/usr/bin/env bash

outdir=~/Videos/youtube-dl
[[ -e "$outdir" ]] || mkdir -p "$outdir" 
notificationtool -o add --icon=icon-lock-transfer --application="Youtube-DL" --urgency=2 -T 2000 "" "" "Video download started in the background."
errmsg=$(youtube-dl -q -o "$outdir/%(title)s_%(id)s.%(ext)s" --exec 'notificationtool -o add --icon=icon-s-accept --application="Youtube-DL" --urgency=2 -T 2000 "" "" "Video download finished, opening..." ; xdg-open {}' "$1" 2>&1 > /dev/null)
ret=$?
if [[ $ret -ne 0 ]]; then
	notificationtool -o add --icon=icon-lock-warning --application="Youtube-DL" --urgency=2 -T 2000 "" "" "Video download failed ($ret) :(" "${errmsg}"
fi

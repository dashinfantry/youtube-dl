import QtQuick 2.0
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0
import Nemo.DBus 2.0

Page {
    id: page

    ConfigurationGroup {
        id: conf
        path: "/org/nephros/youtube-dl/mime-helper"
        property bool enabled
    }

    DBusInterface {
        id: systemd

        bus: DBus.SessionBus
        service: "org.freedesktop.systemd1"
        path: "/org/freedesktop/systemd1"
        iface: "org.freedesktop.systemd1.Manager"
        signalsEnabled: true

        function runService() {
            systemd.typedCall("StartUnit", [ 
              {"type": "s", "value": "youtube-dl-open-installer.service"},
              {"type": "s", "value": "fail"}
              ],
              function(result) { console.log('Systemd call result: ' + result) },
              function(error,message) { console.log('Systemd call failed: ', error, 'message: ', message) }
              )
        }
    }

    SilicaFlickable {
        id: flick
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: parent.width

            PageHeader {
                title: qsTr("Video URL download helper")
            }
            //SectionHeader {
            //    text: qsTr("Toggle")
            //}

            TextSwitch {
                id: videoSwitch
                text: qsTr("Enable downloading video URLs")
                description: qsTr("Enabling this will cause the Browser and other apps to allow you to select a Video Download option for URL links.\n
Videos will be placed in ~/Videos/youtube-dl, and show up in the Media application.\n
Also, we will try to launch the downloaded file afterwards to play the video. This is a security risk if the remote party did something crafty and evil.
Download responsibly.\n
Note that only URLs supported by youtube-dl will work.")

                checked: conf.enabled
                onClicked: {
                  function() { console.log("Toggled!") }
                  systemd.runService()
                }
           }
        }

    }//Flickable
}//Page
